package com.mdi.demo.notification.datanotificationhandler

import com.mdi.demo.notification.Payload
import com.mdi.demo.notification.notificationtypes.*

fun makeNotificationHandler(payload: Payload): DataNotificationHandler {

    return when ( payload.notificationDataType) {
         Deeplink -> {
            DeeplinkNotificationHandler(payload)
        }
        ForceUpdate -> {
            ForceUpdateNotificationHandler(payload)
        }
        OpenLink -> {
            OpenLinkNotificationHandler(payload)
        }
        Maintenance -> {
            MaintenanceNotificationHandler(payload)
        }
        else -> {
            DefaultNotificationHandler(payload)
        }
    }
}