package com.mdi.demo.notification

import android.app.NotificationManager
import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.mdi.demo.notification.mangers.NotificationDataManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class NotificationMangerService : FirebaseMessagingService() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private val parseNotificationDataUseCase = ParseNotificationDataUseCase(Gson())
    private val buildNotificationUseCase = BuildNotificationUseCase(this)

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.data.isNotEmpty()) {
            val payload = parseData(remoteMessage.data)
            if (payload.inAppNotification) {
                val dataIntent = Intent(this, NotificationDataManager::class.java)
                dataIntent.apply {
                    putExtra(NotificationDataManager.PAYLOAD_DATA, payload)
                }
                sendBroadcast(dataIntent)
            } else {
                showNotification(payload)
            }
        }
    }

    private fun parseData(data: Map<String, String>): Payload {
        return parseNotificationDataUseCase.execute(data)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        sendRegistrationToServer(token)
    }

    private fun showNotification(payload: Payload) {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        scope.launch {
            notificationManager.notify(
                payload.id,
                buildNotificationUseCase.execute(payload)
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    private fun sendRegistrationToServer(token: String) {
        // ToDo Add custom implementation, as needed.
    }
}
