package com.mdi.demo.notification.datanotificationhandler

interface DataNotificationHandler {
    fun handle()
}