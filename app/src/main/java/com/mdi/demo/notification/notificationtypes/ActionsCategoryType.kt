package com.mdi.demo.notification.notificationtypes

import androidx.annotation.StringDef

@StringDef(DEFAULT_ACTION_CATEGORY, GENERAL_ACTION_CATEGORY, CARDS_ACTION_CATEGORY, ACCOUNTS_ACTION_CATEGORY)
@Retention(AnnotationRetention.SOURCE)
annotation class ActionsCategoryType
const val DEFAULT_ACTION_CATEGORY = "default"
const val GENERAL_ACTION_CATEGORY = "general"
const val CARDS_ACTION_CATEGORY = "cards"
const val ACCOUNTS_ACTION_CATEGORY = "accounts"