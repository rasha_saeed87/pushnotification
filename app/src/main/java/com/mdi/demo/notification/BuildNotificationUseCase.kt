package com.mdi.demo.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import androidx.core.app.NotificationCompat
import coil.imageLoader
import coil.request.ImageRequest
import com.google.firebase.messaging.FirebaseMessagingService
import com.mdi.demo.R
import com.mdi.demo.notification.mangers.NotificationActionsManger
import com.mdi.demo.notification.mangers.NotificationDataManager


class BuildNotificationUseCase(private val context: Context) {
    suspend fun execute(payload: Payload): Notification {

        val channelId = payload.channel?.id
        val channelName = payload.channel?.name

        if (channelId.isNullOrBlank().not().and(channelName.isNullOrBlank().not())) {
            val notificationManager =
                context.getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val mChannel = NotificationChannel(
                    channelId,
                    channelName,
                    importance
                )
                notificationManager.createNotificationChannel(mChannel)
            }
        }

        val builder = NotificationCompat.Builder(
            context,
            payload.channel?.id ?: context.getString(R.string.default_notification_channel_id)
        )

        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setAutoCancel(true)
        builder.setContentIntent(createDataPendingIntent(payload))
        with(payload) {
            title?.let { builder.setContentTitle(it) }
            body?.let { builder.setContentText(it) }
            actions?.actionsData?.forEach {
                builder.addAction(
                    0,
                    it.name,
                    createActionPendingIntent(actions.category, it, id)
                )
            }
            payload.imageUrl?.let {
                val result =
                    context.imageLoader.execute(ImageRequest.Builder(context).data(it).build())
                ((result.drawable as? BitmapDrawable)?.bitmap)?.let { bitmap ->
                    builder.setStyle(
                        NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmap)
                    )
                }
            }
        }

        return builder.build()
    }

    private fun createActionPendingIntent(
        category: String?,
        action: Action,
        id: Int
    ): PendingIntent {
        val actionIntent = Intent(context, NotificationActionsManger::class.java)
        actionIntent.apply {
            putExtra(NotificationActionsManger.NOTIFICATION_ID, id)
            putExtra(NotificationActionsManger.ACTION_CATEGORY, category)
            putExtra(NotificationActionsManger.ACTION, action)
        }
        return PendingIntent.getBroadcast(
            context,
            action.id,
            actionIntent,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            } else {
                PendingIntent.FLAG_UPDATE_CURRENT
            }
        )
    }

    private fun createDataPendingIntent(payload: Payload): PendingIntent {
        val dataIntent = Intent(context, NotificationDataManager::class.java)
        dataIntent.apply {
            putExtra(NotificationDataManager.PAYLOAD_DATA, payload)
        }
        return PendingIntent.getBroadcast(
            context,
            payload.id,
            dataIntent,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            } else {
                PendingIntent.FLAG_UPDATE_CURRENT
            }
        )
    }

}