package com.mdi.demo.notification

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mdi.demo.notification.Payload

class ParseNotificationDataUseCase(private val gson: Gson) {
    fun execute(data: Map<String, String>): Payload {
        val jsonObject = JsonObject()
        data.forEach {
            try {
                jsonObject.add(it.key, JsonParser.parseString(it.value))
            } catch (e: Exception) {
                jsonObject.addProperty(it.key, it.value)
            }
        }

        val payload = gson.fromJson(jsonObject, Payload::class.java)
        Log.i("NotificationService", payload.toString())
        return payload
    }
}