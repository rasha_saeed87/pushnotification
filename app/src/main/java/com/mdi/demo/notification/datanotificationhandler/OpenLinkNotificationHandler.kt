package com.mdi.demo.notification.datanotificationhandler

import android.util.Log
import com.mdi.demo.notification.Payload

class  OpenLinkNotificationHandler(private val payload: Payload): DataNotificationHandler {
    override fun handle() {
        Log.e("DataNotificationHandler","Open link notification")
    }
}