package com.mdi.demo.notification.notificationtypes

import androidx.annotation.IntDef

@IntDef(Deeplink, ForceUpdate, OpenLink, Maintenance)
@Retention(AnnotationRetention.SOURCE)
annotation class DataType
const val Deeplink = 0
const val ForceUpdate = 1
const val OpenLink = 2
const val Maintenance = 3