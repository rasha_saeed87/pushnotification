package com.mdi.demo.notification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class Payload(
    val id:Int = 0,
    val body: String? = null,
    val title: String? = null,
    val inAppNotification :Boolean = false,
    val notificationDataType :Int?,
    val channel: NotificationChannel? = null,
    @SerializedName("image_url")
    val imageUrl: String? = null,
    val link: String? = null,
    val actions: Actions? = null
):Parcelable
@Parcelize
@Serializable
data class NotificationChannel(
    val id: String? = null,
    val name: String? = null
):Parcelable
@Parcelize
@Serializable
data class Action(
    val id:Int= 0,
    val name: String? = null,
    val deeplink: String? = null,
    val type: String? = null
):Parcelable

@Parcelize
@Serializable
data class Actions(
    val priority: String? = null,
    val category: String? = null,
    @SerializedName("actions_data")
    val actionsData: List<Action>? = null
):Parcelable