package com.mdi.demo.notification.datanotificationhandler

import android.util.Log
import com.mdi.demo.notification.Payload

class DefaultNotificationHandler(private val payload: Payload): DataNotificationHandler {
    override fun handle() {
        Log.e("DataNotificationHandler","Default notification")
    }
}