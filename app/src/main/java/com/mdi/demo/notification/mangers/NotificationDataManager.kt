package com.mdi.demo.notification.mangers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.mdi.demo.notification.Payload
import com.mdi.demo.notification.datanotificationhandler.makeNotificationHandler

class NotificationDataManager : BroadcastReceiver() {


    companion object {
        const val PAYLOAD_DATA = "Data"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val payload: Payload = intent!!.getParcelableExtra(PAYLOAD_DATA)!!
        handleNotificationData(context, payload)
    }

    private fun handleNotificationData(context: Context?, payload: Payload) {

        makeNotificationHandler(payload).handle()
    }
}