package com.mdi.demo.notification.actioncategoryhandler

import android.util.Log
import com.mdi.demo.notification.Action

class GeneralActionCategoryHandler(action: Action): ActionCategoryHandler {
    override fun handle() {
        // TODO handle default action click
        Log.i("ActionCategoryHandler", "GeneralActionCategoryHandler")
    }
}