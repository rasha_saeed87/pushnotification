package com.mdi.demo.notification.mangers

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import com.mdi.demo.notification.Action
import com.mdi.demo.notification.actioncategoryhandler.makeActionCategoryHandler
import com.mdi.demo.notification.notificationtypes.DEFAULT_ACTION_CATEGORY

class NotificationActionsManger : BroadcastReceiver() {
    companion object {
        const val ACTION_CATEGORY = "ACTION_CATEGORY"
        const val ACTION = "ACTION"
        const val NOTIFICATION_ID = "id"

    }

    override fun onReceive(context: Context, intent: Intent) {
        with(intent) {
            val actionCategoryExtra = getStringExtra(ACTION_CATEGORY)
            val actionsCategory: String =
                if (actionCategoryExtra.isNullOrBlank()) DEFAULT_ACTION_CATEGORY else actionCategoryExtra
            val action: Action? = getParcelableExtra(ACTION)
            val notificationId: Int = getIntExtra(NOTIFICATION_ID, 0)
            cancelNotification(context, notificationId)
            if (action != null) {
                makeActionCategoryHandler(actionsCategory, action).handle()
            }
        }
    }

    private fun cancelNotification(
        context: Context,
        notificationId: Int
    ) {
        val notificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(notificationId)
    }
}