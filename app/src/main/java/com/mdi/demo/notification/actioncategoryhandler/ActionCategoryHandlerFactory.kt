package com.mdi.demo.notification.actioncategoryhandler

import com.mdi.demo.notification.Action
import com.mdi.demo.notification.notificationtypes.*

fun makeActionCategoryHandler(
    @ActionsCategoryType actionCategory: String,
    action: Action
): ActionCategoryHandler {
    return when (actionCategory) {
        DEFAULT_ACTION_CATEGORY -> {
            DefaultActionCategoryHandler(action)
        }
        GENERAL_ACTION_CATEGORY -> {
            GeneralActionCategoryHandler(action)
        }
        CARDS_ACTION_CATEGORY -> {
            CardsActionCategoryHandler(action)
        }
        ACCOUNTS_ACTION_CATEGORY -> {
            AccountsActionCategoryHandler(action)
        }
        else -> {
            DefaultActionCategoryHandler(action)
        }
    }
}