package com.mdi.demo.notification.actioncategoryhandler

interface ActionCategoryHandler {
    fun handle()
}